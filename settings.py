import json
import logging

logger = logging.getLogger(__name__)


def apply_defaults(object, defaults):
    """Take default settings and apply them to the object. Attributes which
    already exist, are not overwritten.

    :param object: The object on which the defaults have to be applied.
    :param defaults: A dictionary containing all the default settings.
    """
    for name, value in defaults.items():
        if type(value) == dict:
            if not hasattr(object, name):
                setattr(object, name, SettingsGroup())
                apply_defaults(getattr(object, name), value)
        else:
            if not hasattr(object, name):
                setattr(object, name, value)


def decode_json(object, json_obj):
    """Decode the json dictionary and translate it into attributes.

    :param json_obj: The json object produced by `json.load()`.
    :param object: The object on which you want to create the attributes.
    """
    for name, value in json_obj.items():
        if type(value) == dict:
            setattr(object, name, SettingsGroup())
            decode_json(getattr(object, name), value)
        else:
            setattr(object, name, value)


def encode_json(object):
    """Encode all the setting attributes into a json dict.
    """
    json_dict = {}

    for name, value in object.__dict__.items():
        if type(value) == SettingsGroup:
            json_dict[name] = encode_json(value)
        else:
            json_dict[name] = value

    return json_dict


class SettingsGroup():
    """Class representing a group of settings. This class is used in conjunction
    with the `Settings` class.

    For example in a coffee machine you can have a group of settings related to
    the water heating unit:
     - Max temperature
     - Heating time
     - Water volume
    With this class you could structure your settings according to this example:

    >>> water_settings = SettingsGroup()
    >>> water_settings.max_temp = 70
    >>> water_settings.heating_time = 320
    >>> water_settings.water_volume = None
    >>> water_settings
    """

    def __repr__(self):
        return self.__dict__


class Settings():
    """Generic class which loads settings from disk and saves them to disk.
    The settings are saved in a .json file and saved in a JSON file format.

       :param file_name: The file from which to read the settings. If the file
                         doesn't exist, default values are used.
    """

    def __init__(self, file_name):
        if not file_name.endswith(".json"):
            file_name = f"{file_name}.json"
        self.settings_file = file_name

        # Open the file and create attributes on this object
        try:
            with open(self.settings_file, 'r') as f:
                try:
                    settings_json = json.load(f)
                    decode_json(self, settings_json)
                except json.JSONDecodeError:
                    logger.warning(f"Settings file {file_name} not a valid"
                                   " json file!")
                else:
                    decode_json(self, settings_json)
        except OSError:
            logger.info(f"No existing settings file {file_name} found.")

    def __repr__(self):
        return json.dumps(encode_json(self), sort_keys=True, indent=2)

    def save(self):
        """Save all the JSON settings in a file to disk.
        """
        with open(self.settings_file, 'w+') as f:
            json.dump(encode_json(self), f)


class PiAmplifierSettings(Settings):
    """Class representing all the settings associated with the Raspberry Pi
       amplifier.
    """

    def __init__(self, file_name, defaults):
        super().__init__(file_name)

        # Set default settings if the file does not contain them
        apply_defaults(self, defaults)
        self.save()
