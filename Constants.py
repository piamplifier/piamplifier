# Constants to configure the code, change timing etc here
# How long the button needs to be pushed to activate the menu (in seconds)
Long_push = 1.5
# How long the button needs to be pushed to select something (in seconds)
Short_push = 0.2
ShiftText = 0.25  # Time between two shifts of text on the display (in seconds)
NextShiftTime = 3  # Time before restarting the shift
MENU_ITEMS = ["Select audio input ", "Change displayed info ",
              "Amplifier control ", "Set Volume", "Power"]

# Array containing all possible intpus
INPUTS = {"Aux1": "IN1", "PiAudio": "IN2",
          "Aux2": "IN3", "Aux3": "IN4", "Aux4": "IN5"}

# Amplifier control menu items
AMP_CONTROL_ITEMS = ["Enable amplifier",
                     "Disable amplifier ", "Temperature", "Errors"]

# Options for displayed info
DISPLAYED_INFO_ITEMS = ["MPD", "Kodi", "Selected input"]

# Options for the power menu
POWER_MENU_ITEMS = ["Reboot", "Shut down"]

# Temperature meu items
TEMP_MENU_ITEMS = ["Set temp limit", "Check temperatures "]

# For choosing yes or no
YES_OR_NO = ["yes", "no"]

# GPIO pins
RESET = 23
ROT_INT = 22
DAC_PDOWN = 27
DISABLE_TONEC = 8
INPUT_SEL = 7
DAC_MUTE = 17

AMP_FAULT = 25
AMP_CLIP = 24
AMP_MUTE = 5

DC_PROT1 = 0
DC_PROT2 = 1

# 1602 LCD
LCD_CLK = 16
LCD_CS = 13
LCD_DIN = 20
LCD_DOUT = 26

# LED functions
LED_BOOTED = 8
LED_AMP_ON = 7
LED_TEMP_WARNING = 6
LED_ERROR = 5

LED_AUX1 = 0
LED_AUX2 = 1
LED_AUX3 = 2
LED_AUX4 = 3
LED_PI_AUDIO = 4

# IO Expander GPIOs
PROT_ON = 9

# Match the audio inputs with their LED
INPUT_LED = {'Aux1': LED_AUX1, 'Aux2': LED_AUX2,
             'Aux3': LED_AUX3, 'Aux4': LED_AUX4,
             'PiAudio': LED_PI_AUDIO}

DEFAULT_SETTINGS = {
    "audio": {
        "left_gain": 0,
        "right_gain": 0,
        "bass_gain": 0,
        "mid_gain": 0,
        "treble_gain": 0,
        "volume": 0
    },
    "input": "PiAudio",
    "max_temp": 50
}
