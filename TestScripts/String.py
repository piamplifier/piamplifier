# -*- coding: utf-8 -*-
import smbus
import gaugette.rotary_encoder
import gaugette.switch
import time
from mpd import MPDClient
from pymemcache.client.base import Client

#Rotary encoder pins
A_PIN = 11
B_PIN = 10
SW_PIN = 5

#Initialize encoder
encoder = gaugette.rotary_encoder.RotaryEncoder(A_PIN, B_PIN)
switch = gaugette.switch.Switch(SW_PIN)
#Rotary variables (Timing etc)
Time_pushed = 0
delta = 0
Rotation = None
Button = 0

#Enable shared client
shared = Client(('localhost', 11211))

print ('21' + '°C' + '    ' +  '22' + '°C')
