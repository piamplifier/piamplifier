import time
import RPi.GPIO as GPIO

Period = 0.02
Duty_Cycle = raw_input("Duty Cycle: ")

GPIO.setmode(GPIO.BCM)
GPIO.setup(27, GPIO.OUT)

while True:
    GPIO.output(27, GPIO.HIGH)
    time.sleep(Period * float(Duty_Cycle)/100)
    GPIO.output(27, GPIO.LOW)
    time.sleep(Period - (Period * float(Duty_Cycle)/100))
