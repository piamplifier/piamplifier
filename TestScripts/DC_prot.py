import time
import RPi.GPIO as GPIO

DC_Prot = 16

GPIO.setmode(GPIO.BCM)
GPIO.setup(DC_Prot, GPIO.IN)

while True:
    print(GPIO.input(DC_Prot))
    time.sleep(0.5)
