import RPi.GPIO as GPIO
import time

# Configure the Pi to use the BCM (Broadcom) pin names, rather than the pin pos$
GPIO.setmode(GPIO.BCM)

relays = 14 

GPIO.setup(relays, GPIO.OUT)


try:
    while True:
        GPIO.output(relays, True)  # LED on
        time.sleep(0.5)             # delay 0.5 seconds
        GPIO.output(relays, False) # LED off
        time.sleep(0.5)             # delay 0.5 seconds
finally:
    print("Cleaning up")
    GPIO.cleanup()
