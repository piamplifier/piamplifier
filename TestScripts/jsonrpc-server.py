import socket
from jsonrpc import JSONRPCResponseManager, dispatcher

def complex_function(arg):
  print("Complex echo function. {}".format(arg))

# Dispatcher is dictionary {<method_name>: callable}
dispatcher["echo"] = complex_function
dispatcher["add"] = lambda: print("add")

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
  s.bind(('', 4025))
  s.listen(1)
  conn, addr = s.accept()
  with conn:
    command = ""
    print("Connected to: {}".format(addr))
    while True:
      data = conn.recv(1024)
      if not data:
        break
      data = data.decode("utf-8")
      command += data
      print("Received data: {}".format(data))

      if "\n" in data:
        print("Process data: {}".format(command.strip("\n")))
        response = JSONRPCResponseManager.handle(command.strip("\n"), dispatcher)
        command = ""

    print("Bye!")
