import time
import gaugette.rotary_encoder
import gaugette.switch

A_PIN  = 11
B_PIN  = 10
SW_PIN = 5

encoder = gaugette.rotary_encoder.RotaryEncoder(A_PIN, B_PIN)
switch = gaugette.switch.Switch(SW_PIN)
Time_pushed = 0
delta = 0

while True:
    delta = delta + encoder.get_delta()
    if delta > 3 or delta < -3:
        if delta < 0:
            print "rotate right"
        elif delta > 0:
            print "rotate left"
        delta = 0
            
    sw_state = switch.get_state()
    if sw_state == 1 and Time_pushed == 0:
        print time.time()
        Time_pushed = time.time()

    if sw_state == 0 and Time_pushed != 0:
        Diff = time.time() - Time_pushed
        if Diff > 5:
            print "Button pushed for longer than %d seconds" % Diff
        else:
            print "Didn't push long enough"
        Time_pushed = 0
