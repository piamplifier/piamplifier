import json
import socket, sys

# Example echo method
payload = {
  "method": "disable_amplifier",
  #"params": [35],
  "jsonrpc": "2.0",
}

HOST = 'localhost'    # The remote host
PORT = 4025              # The same port as used by the server
s = None
for res in socket.getaddrinfo(HOST, PORT, socket.AF_UNSPEC, socket.SOCK_STREAM):
  af, socktype, proto, canonname, sa = res
  try:
    s = socket.socket(af, socktype, proto)
  except OSError as msg:
    s = None
    continue
  try:
    s.connect(sa)
  except OSError as msg:
    s.close()
    s = None
    continue
  break
if s is None:
  print('could not open socket')
  sys.exit(1)
with s:
  s.sendall(json.dumps(payload).encode('utf-8'))
  s.sendall('\n'.encode('utf-8'))
  #data = s.recv(1024)
  #print('Received', repr(data))
