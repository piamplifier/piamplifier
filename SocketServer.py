#! /usr/bin/env python
# -*- coding: utf-8 -*-


import time
import os
import threading
import socket
from multiprocessing import Process
import logging

from jsonrpc import JSONRPCResponseManager, dispatcher

from Constants import *
from Enums import *

import gpiozero
import tda7439
import cat9532

debug_logger = logging.getLogger(__name__)


class PiAmpMediaCenter(object):
    """ Object which can be used to control the PiAmplifier.
    """
    error_state = {0x1: "Overheating/Clipping",
                   0x2: "Overload/Undervoltage", 0x3: "Any"}

    def __init__(self):
        """ The default constructor. """
        # Start logging to a file
        logging.basicConfig(level=logging.DEBUG, filename="PiAmplifier.log")

        # Variable to hold the maximum allowed temperature
        self._max_temp = 50

        # Setup the GPIOs with gpiozero
        self._amp_mute = gpiozero.OutputDevice(AMP_MUTE, initial_value=True)
        self._reset = gpiozero.OutputDevice(RESET, initial_value=True)
        self._dac_pdown = gpiozero.OutputDevice(DAC_PDOWN, initial_value=False)
        self._disable_tonec = gpiozero.OutputDevice(
            DISABLE_TONEC, initial_value=False)
        self._input_sel = gpiozero.OutputDevice(INPUT_SEL, initial_value=False)
        self._dac_mute = gpiozero.OutputDevice(DAC_PDOWN, initial_value=True)

        # Define the inputs
        self._amp_fault = gpiozero.DigitalInputDevice(
            AMP_FAULT, active_state=False, bounce_time=0.1)
        self._amp_clip = gpiozero.DigitalInputDevice(
            AMP_CLIP, active_state=False, bounce_time=0.1)
        # Set callback functions
        self._amp_fault.when_activated = self.error_callback
        self._amp_fault.when_deactivated = self.error_callback
        self._amp_clip.when_activated = self.error_callback
        self._amp_clip.when_deactivated = self.error_callback

        self._error = 0x0

        # CAT9532 object to control the LEDs.
        # Light up the booted LED. All the other LEDs are turned off.
        self._io_exp = cat9532.cat9532(address_select=0x7)
        for i in range(8):
            self._io_exp.set_output(i, cat9532.HIGH)
        self._io_exp.set_output(LED_BOOTED, cat9532.LOW)
        self._io_exp.set_output(PROT_ON, cat9532.LOW)

        # Setup the tone control object.
        # Light up the LED of the selected input.
        self._tone_control = tda7439.tda7439(inp_gain=0)
        self._io_exp.set_output(
            INPUT_LED[self._tone_control.input], cat9532.LOW)

        # Activate temperature monitoring with a monitoring interval of 10s
        # self.temp_monitor = FanControl.temperatureMonitor(
        #    self.max_temp, 10, self.temperature_error)
        # self.temp_monitor.start()
        # Used for the temp updater thread
        self.temp_updater = None

        self._amp_muted = None
        self.amplifier_mute = True

        # Enable the DC protection circuit
        # The DC protection circuit needs an update for this to work.
        #self._io_exp.set_output(PROT_ON, cat9532.HIGH)
        #self._io_exp.set_output(PROT_ON, cat9532.LOW)

    @property
    def active_input(self):
        """ Get the current active audio input.

            :see: @active_input.setter
        """
        return self._tone_control.input

    @active_input.setter
    def active_input(self, active_input):
        """ Select a new active audio input.

            :param: active_input One of the available audio inputs
                                 on the tone control board.
                                 Possible values: 'IN1' to 'IN5'
                                 To what they are connected depends
                                 on the hardware configuration.
                                 Currently 'IN2' is PiAudio.
        """
        assert active_input in INPUTS, f"Invalied selected input. Must one of:{INPUTS}"

        # Input 5 is managed through a seperate analog switch.
        if active_input == INPUTS[-1]:
            self._input_sel.on()
            self._tone_control.input = INPUTS[-2]
        else:
            self._input_sel.off()
            self._tone_control.input = active_input

        # Set the corresponding input LED.
        for i in range(5):
            self._io_exp.set_output(i, cat9532.HIGH)
        self._io_exp.set_output(
            INPUT_LED[active_input], cat9532.LOW)

    @property
    def max_temp(self):
        """ Get the currently set maximum amp heatsink temperature.

            :see: @max_temp.setter
        """
        return self._max_temp

    @max_temp.setter
    def max_temp(self, max_temp):
        """ Set the maximum allowed temperature of the amplifier heatsink.

            --------------------------------------------------------------
            Currently this parameter is just a stub. The temperature sensor
            has not yet been implemented.
            --------------------------------------------------------------

            :param: max_temp The maximum allowed heatsink temperature.
                             Must be a value between 35 and 70 degree.

            This parameter determines the maximum allowable temperature of
            the heatsink mounted on top of the amplifier IC.
            Note that this value is much lower than the actual junction
            temperature of the IC. There are many thermal resistances such
            as case-to-junction, case-to-ambient, case-to-heatsink,
            heatsink-to-sensor.
            Set this value to a sensible limit. The IC itself also has an
            internal temperature shutdown function and also overtemperature
            warning.
        """
        if max_temp >= 70 or max_temp <= 35:
            assert max_temp >= 70 and max_temp <= 35, f"Temperature {max_temp} out of range [70, 35]."
        self._max_temp = max_temp
        # Pass the new limit to the temperature monitor
        # self.temp_monitor.setTempLimit(self._max_temp)

    @property
    def amplifier_mute(self):
        """ Get if the amplifier output is muted or note.

            :see: @amplifier_mute.setter
        """
        return self._amp_muted

    @amplifier_mute.setter
    def amplifier_mute(self, mute):
        """ Mute the amplifier.

            :param: mute Mute the amplifier (True or False).

            This property is used to mute the class D output amplifier.
            This makes sure that the speaker will not output any noise
            and that the amplifier goes into a low power state.
            The corresponding amplifier output on/off LED is switched
            on/off when muting/unmuting.
        """
        assert mute in (True, False), f"Mute must be True or False, not {mute}"
        if mute:
            self._amp_mute.on()
            self._dac_pdown.on()
            self._dac_mute.off()
            self._io_exp.set_output(LED_AMP_ON, cat9532.HIGH)
        else:
            self._amp_mute.off()
            self._dac_pdown.off()
            self._dac_mute.on()
            self._io_exp.set_output(LED_AMP_ON, cat9532.LOW)

        self._amp_muted = mute

    @property
    def volume(self):
        """ Get the current volume value.

            :see: @volume.setter
        """
        return self._volume

    @volume.setter
    def volume(self, volume):
        """ Set the volume.

            :param: volume The requested volume between 0 and 100.

            Set the volume to a value between 0 and 100. A volume of
            0 will mute the input signal compeletely.

            The tda7439 tone control IC has a gain and volume setting.
            The gain setting amplifies the input signal up to 30dB.
            The volume setting attenuates the input signal up to -55dB.

            We opted to first use the volume setting and once this setting
            is at 0dB to start increasing the gain. When doing it the other
            way around, there is the risc of clipping the input signal
            before the volume setting is changed. This situation is common
            as sources such as a TV output a large line signal.

            When the volume is 0, a volume attenuation of -56dB is used
            to completely mute the input signal.
            A volume value between 1 and 100 is scaled linearly to a range
            between 0 and 85. Why not just use a volume between 1 and 86?
            Well most systems use a volume between 0 and 100. Why should
            we be different?
            The rescaled volume value is written to the volume attenuation
            as (scaled_volume - 55), as long as the rescaled value is lower
            than 55.
            Once above 55, the volume attenuation is always 0 and the gain
            value is written. The gain value written as (85 - scaled_volume).
        """
        assert volume >= 0 and volume <= 100, f"Invalid volume value: {volume}. Must be between 0 and 100."
        if volume == 0:
            self._tone_control.input_gain = 0
            self._tone_control.volume = 56
        else:
            # Re-scale volume to a value between 0-85.
            scaled_volume = round((volume / 100.0) * 85.0)
            # Calculate the volume and gain values
            if scaled_volume <= 55:
                vol_value = scaled_volume - 55
                gain_value = 0
            else:
                vol_value = 0
                gain_value = 85 - scaled_volume
            # Write values away
            self._tone_control.input_gain = gain_value
            self._tone_control.volume = -vol_value

        self._volume = volume

    @property
    def speaker_gain(self):
        """ Get the gain values set for the individual speaker channels.

            A tuple is returned with the gain value of the left and right
            speaker channel. The gain ranges from -120dB to 0dB.

            :see: @speaker_gain.setter
        """
        return self._tone_control.speaker_l_gain, self._tone_control.speaker_r_gain

    @speaker_gain.setter
    def speaker_gain(self, gain):
        """ Set the same gain for the speaker outputs.

            :param: gain Attenuation value between 0 and 120dB.
                    (So a gain of 0 to -120dB. Sorry for making
                    it confusing.)
        """
        assert gain <= 120 and gain >= 0, f"Wrong speaker gain value {gain}. Must be between 0-120."
        self._tone_control.speaker_l_gain = gain
        self._tone_control.speaker_r_gain = gain

    @property
    def bass_gain(self):
        """ Get the currently configured bass gain.

            :see: @bass_gain.setter
        """
        return self._tone_control.bass_gain

    @bass_gain.setter
    def bass_gain(self, bass_gain):
        """ Set a new bass gain value.

            :param: bass_gain New bass gain value between -14dB and 14dB.
        """
        assert bass_gain <= 14 and bass_gain >= - \
            14, f"Wrong bass gain value {bass_gain}. Must be between -14 and 14."
        self._tone_control.bass_gain = bass_gain

    @property
    def treble_gain(self):
        """ Get the currently configured treble gain.

            :see: @treble_gain.setter
        """
        return self._tone_control.treble_gain

    @treble_gain.setter
    def treble_gain(self, treble_gain):
        """ Set a new treble gain value.

            :param: treble_gain New treble gain value between -14dB and 14dB.
        """
        assert treble_gain <= 14 and treble_gain >= - \
            14, f"Wrong treble gain value {treble_gain}. Must be between -14 and 14."
        self._tone_control.treble_gain = treble_gain

    @property
    def mid_gain(self):
        """ Get the currently configured mid gain.

            :see: @mid_gain.setter
        """
        return self._tone_control.mid_gain

    @mid_gain.setter
    def mid_gain(self, mid_gain):
        """ Set a new mid gain value.

            :param: mid_gain New mid gain value between -14dB and 14dB.
        """
        assert mid_gain <= 14 and mid_gain >= - \
            14, f"Wrong mid gain value {mid_gain}. Must be between -14 and 14."
        self._tone_control.mid_gain = mid_gain

    def error_callback(self):
        """ This method is called when an error is detected.

            The method will update the _error property which can
            then be used to report any errors.

            Also when an error signal is active, it will disable the
            amplifier.
        """
        # Reset any errors
        self._error = 0x0

        # Check which error signals are active
        if (self._amp_clip.is_active):
            self._error |= 0x1

        if (self._amp_fault.is_acitve):
            self._error |= 0x2

        if(self._error != 0x0):
            self.amplifier_mute = True


def socket_handler(pi_amplifier, conn, addr, dispatcher):
    with conn:
        command = ""
        while True:
            data = conn.recv(1024)
            # If the connection is closed stop receiving and abort
            if not data:
                break
            command += data.decode("utf-8")

            # Process the commands if a newline is found
            if '\n' in command:
                command = command.strip("\n")
                logging.debug("Processing received data: {}".format(command))
                print("Processing received data: {}".format(command))
                response = JSONRPCResponseManager.handle(command, dispatcher)

                if response != None:
                    conn.send((str(response.json) + '\n').encode('utf-8'))


if __name__ == '__main__':
    amp = PiAmpMediaCenter()
    # Create a socket object
    host = ''  # socket.gethostname()
    port = 4025
    # Create the dispatcher
    dispatcher["mute_amplifier"] = lambda amp_mute: setattr(
        amp, 'amplifier_mute', amp_mute)
    dispatcher["amplifier_muted"] = lambda: amp.amplifier_mute
    dispatcher["set_temperature"] = lambda new_temp: setattr(
        amp, 'max_temp', new_temp)
    dispatcher["get_temperature"] = lambda: amp.max_temp
    dispatcher["set_input"] = lambda new_input: setattr(
        amp, 'active_input', new_input)
    dispatcher["get_input"] = lambda: amp.active_input
    dispatcher["set_volume"] = lambda volume: setattr(amp, 'volume', volume)
    dispatcher["get_volume"] = lambda: amp.volume
    dispatcher["set_speaker_gain"] = lambda spk_gain: setattr(
        amp, 'speaker_gain', spk_gain)
    dispatcher["get_speaker_gain"] = lambda: amp.speaker_gain
    dispatcher["set_bass_gain"] = lambda bgain: setattr(
        amp, 'bass_gain', bgain)
    dispatcher["get_bass_gain"] = lambda: amp.bass_gain
    dispatcher["set_mid_gain"] = lambda mgain: setattr(amp, 'mid_gain', mgain)
    dispatcher["get_mid_gain"] = lambda: amp.mid_gain
    dispatcher["set_treble_gain"] = lambda tgain: setattr(
        amp, 'treble_gain', tgain)
    dispatcher["get_treble_gain"] = lambda: amp.treble_gain
    dispatcher["get_status"] = lambda: amp.error_state[amp._error]
    dispatcher["version"] = lambda: "PiAmplifier 2.0"
    # Create a new socket server and start listening for connections.
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        # Bind to the port
        sock.bind((host, port))
        while True:
            # Wait for a connection
            sock.listen(1)
            conn, addr = sock.accept()
            logging.debug("Received a connection from: {}".format(addr))
            print("Received a connection from: {}".format(addr))
            # Start a process with the connection
            p = Process(target=socket_handler, args=(
                amp, conn, addr, dispatcher,))
            p.start()
