#! /usr/bin/env python
# -*- coding: utf-8 -*-


import time
import os
import sys
import threading
import socket
from multiprocessing import Process
import logging

import json

# 1602 LCD driver + low level spi driver
import lcd_1602
import spi_intf

# Import all constants and enumerations
from Constants import *
from Enums import *

from w1thermsensor import W1ThermSensor
from mpd import MPDClient

debug_logger = logging.getLogger(__name__)


class LCDInterface():

    def __init__(self):
        """ The default constructor. """
        # Initialize variables
        self.max_temp = 60

        self.currently_displayed = DisplayStates.DISPLAY_INFO
        self.info_displayed = DisplayedInfo.DISPLAY_MPD_INFO
        self.menu_item_selection = 0
        self.input_selection = 0
        self.amp_setting_selection = 0
        self.displayed_info_selection = 0
        self.power_item_selection = 0
        self.temp_item_selection = 0
        self.yes_or_no = 1

        # Create a new LCD object.
        self._spi = spi_intf.SpiLcdIntf(13, 20, 26, 16, lines=2, font_type=0)
        self._lcd = lcd_1602.Lcd1602(self._spi)
        self._lcd.initLCD()

        # Welcome message
        self._lcd.printStringToLCD("Pi powered audio")
        self._lcd.setCursor(1, 0)
        self._lcd.printStringToLCD("player booting...")

        # Used for the temp updater thread
        #self.temp_updater = None

        # Connect to MPD
        self._mpd_client = MPDClient()
        self._mpd_client.timeout = None
        self._mpd_client.idletimeout = None
        self._mpd_client.connect("localhost", 6600)

        status = self._mpd_client.status()
        self._mpd_volume = int(status['volume'])
        self._mpd_state = status['state']

        self._mpd_client.close()
        self._mpd_client.disconnect()

        # Start monitoring for MPD changes
        self.mpdMonitorThread = self.mpdMonitor(self)
        self.mpdMonitorThread.start()

        # Print info to start with
        self.printInfo()

    def send_command(self, method, args=None):
        # Construct Json-RPC payload
        if args is None:
            payload = {
                "method": method,
                "jsonrpc": "2.0",
            }
        else:
            payload = {
                "method": method,
                "params": [args],
                "jsonrpc": "2.0",
            }
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect(('localhost', 4025))
            s.sendall(str.encode(json.dumps(payload)+"\n"))

    def encoderCallback(self, rotation):
        """ This function gets called whenever a rotation was detected from
            the rotary encoder.

        @param rotation   Is 1 or -1 depending on the direction the encoder was rotated.
        """
        # Check at which stage the rotation button is used
        if self.currently_displayed == DisplayStates.DISPLAY_INFO:
            # Check what info is being displayed and take actions
            if self.info_displayed == DisplayedInfo.DISPLAY_MPD_INFO:
                self.client.connect("localhost", 6600)
                # When MPD is selected rotation the button changes songs
                if rotation == 1:
                    self.client.next()
                elif rotation == -1:
                    self.client.previous()
                self.client.close()
                self.client.disconnect()
            if self.info_displayed == DisplayedInfo.DISPLAY_SELECTED_INPUT:
                # Select a different input
                self.input_selection += rotation
                # Check variable boundaries
                if self.input_selection <= -1:
                    self.input_selection = INPUTS_LEN-1
                elif self.input_selection >= INPUTS_LEN:
                    self.input_selection = 0
                self.lcd.printStringAndRotate(1, INPUTS[self.input_selection])
                self.send_command("set_input", INPUTS[self.input_selection])
        elif self.currently_displayed == DisplayStates.DISPLAY_MENU:
            # When in the menu, rotating selects another menu item
            self.menu_item_selection += rotation
            # Check if the number goes out of boundaries
            if self.menu_item_selection <= -1:
                self.menu_item_selection = MENU_ITEMS_LEN-1
            elif self.menu_item_selection >= MENU_ITEMS_LEN:
                self.menu_item_selection = 0
            # Print the new menu item
            self.lcd.printStringAndRotate(
                1, MENU_ITEMS[self.menu_item_selection])
        elif self.currently_displayed == DisplayStates.DISPLAY_SETTING:
            # Input selection
            if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[0]:
                # Select a different input
                self.input_selection += rotation
                # Check variable boundaries
                if self.input_selection <= -1:
                    self.input_selection = INPUTS_LEN-1
                elif self.input_selection >= INPUTS_LEN:
                    self.input_selection = 0
                self.lcd.printStringAndRotate(1, INPUTS[self.input_selection])
                self.send_command("set_input", INPUTS[self.input_selection])
            elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[1]:
                self.displayed_info_selection += rotation
                # Check variable boundaries
                if self.displayed_info_selection <= -1:
                    self.displayed_info_selection = DISPLAYED_INFO_ITEMS_LEN-1
                elif self.displayed_info_selection >= DISPLAYED_INFO_ITEMS_LEN:
                    self.displayed_info_selection = 0
                self.lcd.printStringAndRotate(
                    1, DISPLAYED_INFO_ITEMS[self.displayed_info_selection])
            elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[2]:
                if AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[2]:
                    # Set temperature limit
                    if TEMP_MENU_ITEMS[self.temp_item_selection] == TEMP_MENU_ITEMS[0]:
                        self.max_temp -= 1*rotation
                        # Check variable boundaries
                        if self.max_temp <= 35:
                            self.max_temp = 35
                        elif self.max_temp > 70:
                            self.max_temp = 70
                        self.send_command("set_temperature", self.max_temp)
                        self.lcd.printStringAndRotate(1, str(self.max_temp))
            elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[3]:
                self.mpd_volume -= 2*rotation
                # Check variable boundaries
                if self.mpd_volume <= -1:
                    self.mpd_volume = 0
                elif self.mpd_volume > 100:
                    self.mpd_volume = 100
                # Set volume in MPD
                self.client.connect("localhost", 6600)
                self.client.setvol(self.mpd_volume)
                self.client.close()
                self.client.disconnect()
                self.lcd.printStringAndRotate(1, str(self.mpd_volume))
            elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[4]:
                self.yes_or_no += rotation
                # Check variable boundaries
                if self.yes_or_no <= -1:
                    self.yes_or_no = YES_OR_NO_LEN-1
                elif self.yes_or_no >= YES_OR_NO_LEN:
                    self.yes_or_no = 0
                self.lcd.printStringAndRotate(1, YES_OR_NO[self.yes_or_no])
        elif self.currently_displayed == DisplayStates.DISPLAY_SUBMENU:
            # Amplifier control
            if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[2]:
                self.amp_setting_selection += rotation
                # Check boundaries
                if self.amp_setting_selection <= -1:
                    self.amp_setting_selection = AMP_CONTROL_ITEMS_LEN-1
                elif self.amp_setting_selection >= AMP_CONTROL_ITEMS_LEN:
                    self.amp_setting_selection = 0
                self.lcd.printStringAndRotate(
                    1, AMP_CONTROL_ITEMS[self.amp_setting_selection])
            # Power items (reboot, shutdown)
            elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[4]:
                self.power_item_selection += rotation
                # Check variable boundaries
                if self.power_item_selection <= -1:
                    self.power_item_selection = POWER_MENU_ITEMS_LEN-1
                elif self.power_item_selection >= POWER_MENU_ITEMS_LEN:
                    self.power_item_selection = 0
                self.lcd.printStringAndRotate(
                    1, POWER_MENU_ITEMS[self.power_item_selection])
        elif self.currently_displayed == DisplayStates.DISPLAY_SUBSUBMENU:
            # Temperature submenu
            if AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[2]:
                self.temp_item_selection += rotation
                # Check variable boundaries
                if self.temp_item_selection <= -1:
                    self.temp_item_selection = TEMP_MENU_ITEMS_LEN-1
                elif self.temp_item_selection >= TEMP_MENU_ITEMS_LEN:
                    self.temp_item_selection = 0
                self.lcd.printStringAndRotate(
                    1, TEMP_MENU_ITEMS[self.temp_item_selection])

    def switchISR(self):
        """ This interrupt service routine gets called when an edge was detected
            on the switch pin.
        """
        # Check the push button state
        sw_state = self.switch.get_state()
        if sw_state == 1 and self.time_pushed == 0:
            self.time_pushed = time.time()  # When the button is pushed save the current time
        if sw_state == 0 and self.time_pushed == 0:
            button_pushed = 0
        # Check how long the button is pushed.
        if sw_state == 0 and self.time_pushed != 0:
            self.diff = time.time() - self.time_pushed
            if self.diff > Long_push:
                # Check what is currently being displayed
                if self.currently_displayed == DisplayStates.DISPLAY_INFO:
                    # Display menu
                    self.lcd.printStringAndRotate(0, "Menu:")
                    self.lcd.printStringAndRotate(
                        1, MENU_ITEMS[self.menu_item_selection])
                    self.currently_displayed = DisplayStates.DISPLAY_MENU
                elif self.currently_displayed == DisplayStates.DISPLAY_MENU:
                    # Long push in the menu means exit menu
                    self.printInfo()
                    self.currently_displayed = DisplayStates.DISPLAY_INFO
            # Short push means select element or take action
            elif self.diff > Short_push:
                # Check what is currently being displayed
                if self.currently_displayed == DisplayStates.DISPLAY_INFO:
                    # Check what info is being displayed (Kodi info, MPD info,...)
                    if self.info_displayed == DisplayedInfo.DISPLAY_MPD_INFO:
                        self.client.connect("localhost", 6600)
                        status = self.client.status()
                        self.mpd_volume = int(status['volume'])
                        # Different actions depending on the players state
                        if status['state'] == 'play':
                            self.client.pause(1)
                        elif status['state'] == 'pause':
                            self.client.pause(0)
                        else:
                            self.client.play(0)
                        self.client.close()
                        self.client.disconnect()
                # A short push selects a menu item
                elif self.currently_displayed == DisplayStates.DISPLAY_MENU:
                    # Input selection
                    if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[0]:
                        self.currently_displayed = DisplayStates.DISPLAY_SETTING
                        self.lcd.printStringAndRotate(0, "Selected input:")
                        self.lcd.printStringAndRotate(
                            1, INPUTS[self.input_selection])
                        self.send_command(
                            "set_input", INPUTS[self.input_selection])
                    # Displayed info options menu
                    if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[1]:
                        self.currently_displayed = DisplayStates.DISPLAY_SETTING
                        self.lcd.printStringAndRotate(0, "Displayed info:")
                        self.lcd.printStringAndRotate(
                            1, DISPLAYED_INFO_ITEMS[self.displayed_info_selection])
                    # Amplifier options sub menu
                    elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[2]:
                        self.currently_displayed = DisplayStates.DISPLAY_SUBMENU
                        self.lcd.printStringAndRotate(0, "Amp settings:")
                        self.lcd.printStringAndRotate(
                            1, AMP_CONTROL_ITEMS[self.amp_setting_selection])
                    # Set volume
                    elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[3]:
                        self.currently_displayed = DisplayStates.DISPLAY_SETTING
                        self.lcd.printStringAndRotate(0, "Set volume:")
                        self.lcd.printStringAndRotate(1, str(self.mpd_volume))
                    elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[4]:
                        self.currently_displayed = DisplayStates.DISPLAY_SUBMENU
                        self.lcd.printStringAndRotate(0, "Do you want to:")
                        self.lcd.printStringAndRotate(
                            1, POWER_MENU_ITEMS[self.power_item_selection])
                elif self.currently_displayed == DisplayStates.DISPLAY_SUBMENU:
                    # Amplifier control
                    if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[2]:
                        # Enable amplifier
                        if AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[0]:
                            self.lcd.printStringAndRotate(
                                0, "Enabling amplifier")
                            self.lcd.printStringAndRotate(1, "please wait...")
                            self.send_command("enable_amplifier")
                            time.sleep(1)
                            self.printInfo()
                        # Disable amplifier
                        elif AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[1]:
                            self.send_command("disable_amplifier")
                            self.printInfo()
                        elif AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[2]:
                            self.currently_displayed = DisplayStates.DISPLAY_SUBSUBMENU
                            self.lcd.printStringAndRotate(0, "Do you want to:")
                            self.lcd.printStringAndRotate(
                                1, TEMP_MENU_ITEMS[self.temp_item_selection])
                    if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[4]:
                        self.currently_displayed = DisplayStates.DISPLAY_SETTING
                        self.lcd.printStringAndRotate(0, "Are you sure?")
                        self.lcd.printStringAndRotate(
                            1, YES_OR_NO[self.yes_or_no])
                elif self.currently_displayed == DisplayStates.DISPLAY_SUBSUBMENU:
                    # Power submenu
                    if AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[2]:
                        # Set temperature limit
                        if TEMP_MENU_ITEMS[self.temp_item_selection] == TEMP_MENU_ITEMS[0]:
                            self.currently_displayed = DisplayStates.DISPLAY_SETTING
                            self.lcd.printStringAndRotate(0, "Shutoff temp:")
                            self.lcd.printStringAndRotate(
                                1, str(self.max_temp))
                        # Read out temperatures
                        if TEMP_MENU_ITEMS[self.temp_item_selection] == TEMP_MENU_ITEMS[1]:
                            self.currently_displayed = DisplayStates.DISPLAY_SETTING
                            # Start a thread that displays the current temperature and updates it every 5s
                            if self.temp_updater is None:
                                self.temp_updater = self.temperatureUpdater(
                                    self, 5)
                                self.temp_updater.start()
                            elif self.temp_updater.isStopped():
                                self.temp_updater = self.temperatureUpdater(
                                    self, 5)
                                self.temp_updater.start()
                # Exit the setting menu
                elif self.currently_displayed == DisplayStates.DISPLAY_SETTING:
                    # Select displayed info
                    if MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[1]:
                        if DISPLAYED_INFO_ITEMS[self.displayed_info_selection] == DISPLAYED_INFO_ITEMS[0]:
                            self.info_displayed = DisplayedInfo.DISPLAY_MPD_INFO
                        elif DISPLAYED_INFO_ITEMS[self.displayed_info_selection] == DISPLAYED_INFO_ITEMS[1]:
                            self.info_displayed = DisplayedInfo.DISPLAY_KODI_INFO
                        elif DISPLAYED_INFO_ITEMS[self.displayed_info_selection] == DISPLAYED_INFO_ITEMS[2]:
                            self.info_displayed = DisplayedInfo.DISPLAY_SELECTED_INPUT
                    # Amplifier control
                    elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[2]:
                        if AMP_CONTROL_ITEMS[self.amp_setting_selection] == AMP_CONTROL_ITEMS[2]:
                            if TEMP_MENU_ITEMS[self.temp_item_selection] == TEMP_MENU_ITEMS[1]:
                                self.currently_displayed = DisplayStates.DISPLAY_INFO
                                if self.temp_updater is not None:
                                    self.temp_updater.stop()
                                    self.temp_updater.join()
                                    self.temp_updater = None
                    # Power
                    elif MENU_ITEMS[self.menu_item_selection] == MENU_ITEMS[4]:
                        self.lcd.clearLCD()
                        # Reboot the system
                        if POWER_MENU_ITEMS[self.power_item_selection] == POWER_MENU_ITEMS[0]:
                            if YES_OR_NO[self.yes_or_no] == YES_OR_NO[0]:
                                self.lcd.printStringAndRotate(
                                    0, 'Rebooting...')
                                os.system('sudo reboot')
                        # Shut down the system
                        elif POWER_MENU_ITEMS[self.power_item_selection] == POWER_MENU_ITEMS[1]:
                            if YES_OR_NO[self.yes_or_no] == YES_OR_NO[0]:
                                self.lcd.printStringAndRotate(
                                    0, 'Shutting down..')
                                os.system('sudo shutdown -h now')
                    self.printInfo()
                elif self.currently_displayed == DisplayStates.DISPLAY_TEMP_ERROR:
                    self.temp_monitor.resetTempError()
                    self.currently_displayed == DisplayStates.DISPLAY_INFO
                    self.printInfo()
            # ToDo: Check if nescessary
            self.time_pushed = 0

    def printInfo(self):
        self.currently_displayed = DisplayStates.DISPLAY_INFO
        if self.info_displayed == DisplayedInfo.DISPLAY_MPD_INFO:
            self.printMPDInfo()
        elif self.info_displayed == DisplayedInfo.DISPLAY_KODI_INFO:
            self._lcd.printStringAndRotate(0, "Kodi has not yet")
            self._lcd.printStringAndRotate(1, "been implemented")
        elif self.info_displayed == DisplayedInfo.DISPLAY_SELECTED_INPUT:
            self._lcd.printStringAndRotate(0, "Selected input:")
            self._lcd.printStringAndRotate(
                1, "AuxS")

    def printMPDInfo(self):
        self._mpd_client.connect("localhost", 6600)
        # Read in current song
        song = self._mpd_client.currentsong()
        # Read in player status
        status = self._mpd_client.status()
        self._mpd_client.close()
        self._mpd_client.disconnect()

        if status['state'] == 'play':
            self._lcd.printStringAndRotate(
                0, song['title'] + "-" + song['artist'])

            TimeSplit = status['time'].split(':')
            # Get the elapsed time and the song duration
            MinElapsed, SecElapsed = divmod(
                int(TimeSplit[0]), 60)  # Split minutes and seconds
            MinDuration, SecDuration = divmod(int(TimeSplit[1]), 60)
            # Append a zero if the value is less than ten
            if SecElapsed < 10:
                SecElapsed = '0' + str(SecElapsed)
            else:
                SecElapsed = str(SecElapsed)
            if SecDuration < 10:
                SecDuration = '0' + str(SecDuration)
            else:
                SecDuration = str(SecDuration)
            self._lcd.printStringAndRotate(1, str(
                MinElapsed) + ':' + SecElapsed + '/' + str(MinDuration) + ':' + SecDuration)

        elif status['state'] == 'pause':
            self._lcd.printStringAndRotate(
                0, song['title'] + "-" + song['artist'])
            self._lcd.printStringAndRotate(1, "Paused")

        elif status['state'] == 'stop':
            self._lcd.printStringAndRotate(0, "MPD media player")
            self._lcd.printStringAndRotate(1, "Stopped")

    class mpdMonitor(threading.Thread):
        """ Threading class used to monitor for changes in MPD and
            report them in the system output.
        """

        def __init__(self, LCDInterface):
            """ Default constructor. """
            super(LCDInterface.mpdMonitor, self).__init__()
            self._mpd_client = MPDClient()
            self._mpd_client.timeout = None
            self._mpd_client.idletimeout = None
            self.LCDInterface = LCDInterface

        def run(self):
            while True:
                # Connect to MPD and wait for an event.
                self._mpd_client.connect("localhost", 6600)
                self._mpd_client.idle()
                self._mpd_client.close()
                self._mpd_client.disconnect()
                if self.LCDInterface.currently_displayed == DisplayStates.DISPLAY_INFO:
                    if self.LCDInterface.info_displayed == DisplayedInfo.DISPLAY_MPD_INFO:
                        self.LCDInterface.printMPDInfo()

    class temperatureUpdater(threading.Thread):
        """ Threading class used to read out the internal amplifier
            temperature and print it to the display.
        """

        def __init__(self, LCDInterface, update_interval):
            """ Default constructor. """
            super().__init__()
            self.thread_stopped = threading.Event()
            self.thread_stopped.set()
            self.update_interval = update_interval
            self.LCDInterface = LCDInterface

        def run(self):
            # Setup the DS18B20 connection
            self.temp_sensors = []
            for sensor in W1ThermSensor.get_available_sensors():
                self.temp_sensors += [sensor]
            """ The temperature is printed to the display until the thread
                is explicitly stopped. """
            self.thread_stopped.clear()
            while not self.isStopped():
                if len(self.temp_sensors) >= 2:
                    self.LCDInterface.lcd.printStringAndRotate(
                        0, "Temp1: " + str(self.temp_sensors[0].get_temperature()) + chr(0xDF) + "C")
                    self.LCDInterface.lcd.printStringAndRotate(
                        1, "Temp2: " + str(self.temp_sensors[1].get_temperature()) + chr(0xDF) + "C")
                elif len(self.temp_sensors) == 1:
                    self.LCDInterface.lcd.clearLCD()
                    self.LCDInterface.lcd.printStringAndRotate(
                        0, "Temp: " + str(self.temp_sensors[0].get_temperature()) + chr(0xDF) + "C")
                else:
                    self.LCDInterface.lcd.printStringAndRotate(
                        0, "No temperature")
                    self.LCDInterface.printStringAndRotate(
                        1, "sensor installed")
                # Check if the thread is stopped
                if self.isStopped():
                    return
                # Update interval to keep CPU load low
                time.sleep(self.update_interval)

        def stop(self):
            """ Stop the temperature updater thread. """
            self.thread_stopped.set()

        def isStopped(self):
            """ Returns if the thread is stopped or not.

            @retval True:   The thread is stopped.
            @retval False:  The thread is running.
            """
            return self.thread_stopped.is_set()


if __name__ == '__main__':
    # Start logging to a file
    logging.basicConfig(level=logging.WARNING, filename="LCDInterface.log")
    intf = LCDInterface()
    while True:
        time.sleep(100)
