import settings
import unittest
import json
import os

default_settings = {
    "audio": {
        "bass_gain": 0,
        "mid_gain": 0,
        "treble_gain": 0,
        "volume": 0
    },
    "input": "PiAudio",
    "max_temp": 50
}


class JSONEncodeDecode(unittest.TestCase):
    def test_decode(self):
        """Test if the decode json function can decode a json file and set the
        right attributes.
        """
        class dummy():
            """Dummy class
            """

        d = dummy()
        json_obj = None
        with open("test.json") as f:
            json_obj = json.load(f)
            settings.decode_json(d, json_obj)

        self.assertEqual(d.glossary.title, "example glossary")
        self.assertEqual(d.glossary.GlossDiv.title, "S")
        self.assertEqual(d.glossary.GlossDiv.GlossList.GlossEntry.ID, "SGML")
        self.assertEqual(
            d.glossary.GlossDiv.GlossList.GlossEntry.SortAs, "SGML")
        self.assertEqual(d.glossary.GlossDiv.GlossList.GlossEntry.GlossTerm,
                         "Standard Generalized Markup Language")
        self.assertEqual(
            d.glossary.GlossDiv.GlossList.GlossEntry.Acronym, "SGML")
        self.assertEqual(
            d.glossary.GlossDiv.GlossList.GlossEntry.Abbrev, "ISO 8879:1986")
        self.assertEqual(
            d.glossary.GlossDiv.GlossList.GlossEntry.GlossDef.GlossSeeAlso, ["GML", "XML"])

        return d, json_obj

    def test_encode(self):
        """Use the object and json dict from test_decode to test the encoding
        function. It should output the same json dict as json.load.
        """
        obj, json_obj = self.test_decode()

        json_dict = settings.encode_json(obj)

        self.assertEqual(json_dict, json_obj)


class TestSettings(unittest.TestCase):
    def setUp(self):
        self.settings = settings.PiAmplifierSettings("test.json", defaults={})

    def test_save_recall(self):
        """Modify the settings. Save them to a file and recall them.
        """
        self.settings.volume = 22
        self.settings.mid_gain = 23
        self.settings.treble_gain = 44
        self.settings.bass_gain = 66
        self.settings.save()

        del self.settings

        amp_settings = settings.PiAmplifierSettings("test.json", defaults={})

        self.assertEqual(amp_settings.volume, 22)
        self.assertEqual(amp_settings.mid_gain, 23)
        self.assertEqual(amp_settings.treble_gain, 44)
        self.assertEqual(amp_settings.bass_gain, 66)

    def test_default_settings(self):
        """Test if the defaults we give to the constructor are applied as
        expected.
        """
        if os.path.exists("test_defaults.json"):
            os.remove("test_defaults.json")

        def_set = settings.PiAmplifierSettings(
            "test_defaults", default_settings)
        del def_set.settings_file

        self.assertEqual(settings.encode_json(def_set), default_settings)


if __name__ == '__main__':
    unittest.main()
