"""
    Python-I2C-1602-LCD-Driver
    Copyright (C) 2018, 2019 Tijl Schepens

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

#! /usr/bin/env python
# -*- coding: utf-8 -*-

import time

import RPi.GPIO as GPIO
from w1thermsensor import W1ThermSensor
import threading

from Constants import *

"""@package docstring
This module can be used to control Fans using PWM control on the Raspberry Pi.

"""

class FanControl:
    """ Class used to control cooling fans. 
    """
    def __init__(self, pin, pwm_freq, start_duty):
        """ The constructor initializes private variables and sets up the fans pin
            as PWM pin.

        @param pin          The pin (BCM pinout) to which the fans are connected.
        @param pwm_freq     Frequency used to control the fans. (50Hz is a good choice)
        @param start_duty   Duty-cycle used after start-up. Note that 40% is usually a
                            minimum. Lower than that and the fans act weird.
        """
        self.pin = pin
        self.frequency = pwm_freq
        self.duty_cycle = start_duty
        GPIO.setmode(GPIO.BCM)
        # Set up GPIO
        GPIO.setup(pin, GPIO.OUT)

        # Set up PWM for the fan control
        self.fan = GPIO.PWM(self.pin, self.frequency)
        self.fan.start(self.duty_cycle)

    def __del__(self):
        """ The destructor stops the fans.
        """
        self.stopFans()

    def changeFrequency(self, freq):
        """ Change the frequency used for the PWM signal.

        @param freq   The frequency used in Hz.
        """
        self.frequency = freq
        self.fan.ChangeFrequency(freq)

    def setDutyCycle(self, duty_cycle):
        """ Change the duty-cycle of the PWM signal.

        @param duty_cycle   The desired duty-cycle (0-100).
        """
        self.duty_cycle = duty_cycle
        self.fan.ChangeDutyCycle(self.duty_cycle)

    def stopFans(self):
        """ Stop the PWM signal. """
        self.fan.stop()

    def startFans(self, duty_cycle=None):
        """ Change the duty-cycle of the PWM signal.

        @param duty_cycle   The desired duty-cycle (0-100). (Optional)
        """
        if duty_cycle is not None:
            self.duty_cycle = duty_cycle

        # Start the fans
        self.fan.start(self.duty_cycle)
        self.fan.ChangeFrequency(self.frequency)

        
class temperatureMonitor(threading.Thread):
    """ Threading class used to monitor the internal temperature
        of the amplifier and shuts down the amplifier in case of
        overtemperature.
    """
    def __init__(self, max_temp, monitor_interval, callback):
        """ Default constructor. """
        super().__init__()
        self.monitor_interval = monitor_interval
        ## Indicated if temperature error mode is active
        self.temp_error_mode = False
        # Set up the fan control
        self.fans = FanControl(FANS, 25, 0)
        self.fans.stopFans()
        ## Are the fans started or not?
        self.fans_started = False
        self.max_temp = max_temp
        self.overtemp_callback = callback

    def run(self):
        # Setup the DS18B20 connection
        self.temp_sensors = []
        for sensor in W1ThermSensor.get_available_sensors():
            self.temp_sensors += [sensor]
        # Check if sensors are present
        if len(self.temp_sensors) == 0:
            return
        while True:
            highest_temp = 0
            for sensor in self.temp_sensors:
                temp = sensor.get_temperature()
                # Keep track of the highest temperature
                if temp > highest_temp:
                    highest_temp = temp
                if not self.temp_error_mode:
                    if temp >= self.max_temp:
                        # Activate the callback function
                        self.overtemp_callback()
                        # Active temp error mode
                        self.temp_error_mode = True
            # If the temperature is too high start the fans
            if highest_temp >= 40:
                if self.fans_started == False:
                    self.fans.startFans(50 + int((highest_temp-40)*50/20))
                    self.fans_started = True
                else:
                    self.fans.setDutyCycle(50 + int((highest_temp-40)*50/20))
            else:
                if self.fans_started == True:
                    self.fans.stopFans()
                    self.fans_started = False
            # Monitoring interval to keep the CPU load low
            time.sleep(self.monitor_interval)

    def resetTempError(self):
        """ Reset the temperature error mode. """
        self.temp_error_mode = False

    def setTempLimit(self, new_temp):
        """ Set a new temperature limit.
        """
        self.max_temp = new_temp

    def isTempErrorModeActive(self):
        """ Returns if temperature error mode is active.

        @retval True:   Temperature error mode is active.
        @retval False:  Temperature error mode is not active.
        """
        return self.temp_error_mode
