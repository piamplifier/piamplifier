from enum import Enum

class DisplayStates(Enum):
    DISPLAY_INFO = 0
    DISPLAY_MENU = 1
    DISPLAY_SUBMENU = 2
    DISPLAY_SUBSUBMENU = 3
    DISPLAY_SETTING = 4
    DISPLAY_TEMP_ERROR = 5

class DisplayedInfo(Enum):
    DISPLAY_MPD_INFO = 0
    DISPLAY_KODI_INFO = 1
    DISPLAY_SELECTED_INPUT = 2

class LedStates(Enum):
    OFF = 0
    ON = 1
